package cz.dennistschamler.survey.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.util.Calendar;
import java.util.Date;

@ActiveProfiles(profiles = "test")
public class DateUtilsTest {
    private static final long MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000;

    @Test
    public void whenGettingDateWithoutTime_thenReturnDateWithoutTime() {
        Date dateWithoutTime = DateUtils.getCurrentDateWithoutTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateWithoutTime);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTimeInMillis(dateWithoutTime.getTime() + MILLISECONDS_PER_DAY - 1);
        Assertions.assertEquals(day, calendar.get(Calendar.DAY_OF_MONTH));

        calendar.setTimeInMillis(dateWithoutTime.getTime() + MILLISECONDS_PER_DAY);
        Assertions.assertNotEquals(day, calendar.get(Calendar.DAY_OF_MONTH));
    }
}
