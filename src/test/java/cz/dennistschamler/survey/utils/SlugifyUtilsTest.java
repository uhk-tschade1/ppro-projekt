package cz.dennistschamler.survey.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles(profiles = "test")
public class SlugifyUtilsTest {

    @Test
    public void whenSlugifyingString_thenReturnSlugifiedString() {
        String a = "Test as asd asd sad ";
        Assertions.assertEquals("test-as-asd-asd-sad", SlugifyUtils.slugify(a));

        String b = "ěščřž žýáčř";
        Assertions.assertEquals("escrz-zyacr", SlugifyUtils.slugify(b));

        String c = "           abc                    def            ";
        Assertions.assertEquals("abc-def", SlugifyUtils.slugify(c));
    }
}
