package cz.dennistschamler.survey.model.entity.role;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ActiveProfiles("test")
public class RoleRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void whenFindByName_thenReturnRole() {
        // given
        Role guest = new Role();
        guest.setName("ROLE_GUEST");
        entityManager.persist(guest);
        entityManager.flush();

        // when
        Role found = roleRepository.findByName(guest.getName());

        // then
        Assertions.assertThat(found.getName())
                .isEqualTo(guest.getName());
    }

}
