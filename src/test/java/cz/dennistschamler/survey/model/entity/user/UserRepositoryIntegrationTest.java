package cz.dennistschamler.survey.model.entity.user;

import cz.dennistschamler.survey.model.entity.role.Role;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByEmail_thenReturnUser() {
        // given
        Role testRole = new Role();
        testRole.setName("ROLE_TEST");
        entityManager.persist(testRole);

        User user = new User();
        user.setEmail("test@test.cz");
        user.setRole(testRole);
        user.setPassword("password");
        user.setFirstName("Josef");
        user.setLastName("Novák");
        entityManager.persist(user);

        entityManager.flush();

        // when
        User found = userRepository.findOneByEmail(user.getEmail());

        // then
        Assertions.assertThat(found.getEmail())
                .isEqualTo(user.getEmail());
    }

}
