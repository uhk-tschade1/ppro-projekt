package cz.dennistschamler.survey.controller.admin;

import cz.dennistschamler.survey.model.entity.user.IUserService;
import cz.dennistschamler.survey.model.entity.user.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
@WithMockUser(username = "test@test.cz", roles = {"ADMIN"})
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private IUserService userService;

    @Test
    @DisplayName("GET /admin/users success with authenticated user")
    public void testUsers() throws Exception {
        this.mockMvc.perform(get("/admin/users")).andExpect(status().isOk());
    }

    @Test
    @DisplayName("GET /admin/users/-1 redirect to /admin/users/0")
    public void testUsersInvalidPage() throws Exception {
        this.mockMvc.perform(get("/admin/users/-1")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/admin/users/0"));
    }

    @Test
    @DisplayName("GET /admin/users redirect to /sign-in with anonymous user")
    @WithAnonymousUser
    public void testUsersAnonymous() throws Exception {
        this.mockMvc.perform(get("/admin/users")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("http://localhost/sign-in"));
    }

    @Test
    @DisplayName("GET /admin/users/detail/<id> success with authenticated user")
    public void testUserDetail() throws Exception {
        User user = userService.findLatestUser();
        this.mockMvc.perform(get("/admin/users/detail/" + user.getId())).andExpect(status().isOk());
    }

    @Test
    @DisplayName("GET /admin/users/detail/-1 redirect to /admin/users")
    public void testUserDetailInvalidUserId() throws Exception {
        this.mockMvc.perform(get("/admin/users/detail/-1")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/admin/users"));
    }

    @Test
    @DisplayName("GET /admin/users/detail/<id> redirect to /sign-in with anonymous user")
    @WithAnonymousUser
    public void testUserDetailAnonymous() throws Exception {
        User user = userService.findLatestUser();
        this.mockMvc.perform(get("/admin/users/detail/" + user.getId())).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("http://localhost/sign-in"));
    }
}
