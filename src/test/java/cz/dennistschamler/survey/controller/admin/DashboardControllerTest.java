package cz.dennistschamler.survey.controller.admin;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
@WithMockUser(username="test@test.cz",roles={"ADMIN"})
public class DashboardControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("GET /admin success with authenticated user")
    public void testDashboard() throws Exception {
        this.mockMvc.perform(get("/admin")).andExpect(status().isOk());
    }

    @Test
    @DisplayName("GET /admin redirect to /sign-in with anonymous user")
    @WithAnonymousUser
    public void testDashboardAnonymous() throws Exception {
        this.mockMvc.perform(get("/admin")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("http://localhost/sign-in"));
    }
}