package cz.dennistschamler.survey.controller.admin;

import cz.dennistschamler.survey.model.entity.survey.ISurveyService;
import cz.dennistschamler.survey.model.entity.survey.Survey;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
@WithMockUser(username="test@test.cz",roles={"ADMIN"})
public class AdminSurveyControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ISurveyService surveyService;

    @Test
    @DisplayName("GET /admin/surveys success with authenticated user")
    public void testSurveys() throws Exception {
        this.mockMvc.perform(get("/admin/surveys")).andExpect(status().isOk());
    }

    @Test
    @DisplayName("GET /admin/surveys/-1 redirect to /admin/surveys/0")
    public void testSurveysInvalidPage() throws Exception {
        this.mockMvc.perform(get("/admin/surveys/-1")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/admin/surveys/0"));
    }

    @Test
    @DisplayName("GET /admin/surveys redirect to /sign-in with anonymous user")
    @WithAnonymousUser
    public void testSurveysAnonymous() throws Exception {
        this.mockMvc.perform(get("/admin/surveys")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("http://localhost/sign-in"));
    }

    @Test
    @DisplayName("GET /admin/surveys/detail/10 success with authenticated user")
    public void testSurveyDetail() throws Exception {
        Survey survey = surveyService.findLatestSurvey();
        this.mockMvc.perform(get("/admin/surveys/detail/"+survey.getId())).andExpect(status().isOk());
    }

    @Test
    @DisplayName("GET /admin/surveys/detail/-1 redirect to /admin/surveys")
    public void testSurveyDetailInvalidUserId() throws Exception {
        this.mockMvc.perform(get("/admin/surveys/detail/-1")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/admin/surveys"));
    }

    @Test
    @DisplayName("GET /admin/surveys/detail/10 redirect to /sign-in with anonymous user")
    @WithAnonymousUser
    public void testSurveyDetailAnonymous() throws Exception {
        Survey survey = surveyService.findLatestSurvey();
        this.mockMvc.perform(get("/admin/surveys/detail/"+survey.getId())).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("http://localhost/sign-in"));
    }
}
