package cz.dennistschamler.survey.controller.front;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class FrontSurveyControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("GET /surveys success")
    public void testSurveys() throws Exception {
        this.mockMvc.perform(get("/surveys")).andExpect(status().isOk());
    }

    @Test
    @DisplayName("GET /surveys/detail/ success")
    public void testSurveyDetail() throws Exception {
        this.mockMvc.perform(get("/surveys")).andExpect(status().isOk());
    }
}
