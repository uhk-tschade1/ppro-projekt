package cz.dennistschamler.survey.controller.front;

import cz.dennistschamler.survey.model.entity.survey.ISurveyService;
import cz.dennistschamler.survey.model.entity.survey.Survey;
import cz.dennistschamler.survey.model.sitemap.Sitemap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Controller
public class SitemapController extends BaseFrontController {

    private ISurveyService surveyService;

    @GetMapping(value = "/sitemap.xml", produces = {MediaType.APPLICATION_XML_VALUE})
    public @ResponseBody
    Sitemap index(HttpServletRequest req) {
        String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();;

        Sitemap sitemap = new Sitemap(baseUrl);

        sitemap.addUrl("").setPriority(0.8f).setChangeFrequency(Sitemap.ChangeFrequency.DAILY);

        Survey survey = surveyService.findLatestSurvey();

        Sitemap.Url url = sitemap.addUrl("surveys").setChangeFrequency(Sitemap.ChangeFrequency.DAILY).setPriority(0.8f);

        if(survey != null) {
            url.setLastModified(Timestamp.valueOf(survey.getCreatedAt()));
        }

        List<Survey> surveys = surveyService.findAll();

        for(Survey s : surveys) {
            // Include only ongoing surveys
            // TODO: Fetch already ended surveys directly from database by query
            if(s.hasEnded()) continue;

            sitemap
                    .addUrl("surveys/detail/"+s.getId()+"/"+s.getSlug())
                    .setPriority(0.9f)
                    .setChangeFrequency(Sitemap.ChangeFrequency.WEEKLY)
                    .setLastModified(Timestamp.valueOf(s.getUpdatedAt()));
        }

        sitemap.addUrl("sign-in").setPriority(0.4f).setChangeFrequency(Sitemap.ChangeFrequency.MONTHLY);
        sitemap.addUrl("sign-up").setPriority(0.4f).setChangeFrequency(Sitemap.ChangeFrequency.MONTHLY);

        return sitemap;
    }

    @Autowired
    public void setSurveyService(ISurveyService surveyService) {
        this.surveyService = surveyService;
    }
}
