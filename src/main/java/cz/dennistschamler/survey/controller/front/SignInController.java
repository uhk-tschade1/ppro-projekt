package cz.dennistschamler.survey.controller.front;

import cz.dennistschamler.survey.model.Breadcrumb;
import cz.dennistschamler.survey.model.dto.UserSignInDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class SignInController extends BaseFrontController {

    private AuthenticationManager authenticationManager;

    @GetMapping("/sign-in")
    public String signIn(@ModelAttribute("breadcrumbs") List<Breadcrumb> breadcrumbs, Model model) {
        model.addAttribute("userSignInDto", new UserSignInDto());
        breadcrumbs.add(new Breadcrumb("Sign in", ""));
        return "front/sign-in";
    }

    @PostMapping(value = "/sign-in")
    public String signInSubmit(@Valid @ModelAttribute("userSignInDto") UserSignInDto userSignInDto, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors()) {
            return "front/sign-in";
        }

        UsernamePasswordAuthenticationToken authReq
                = new UsernamePasswordAuthenticationToken(userSignInDto.getEmail(), userSignInDto.getPassword());
        Authentication auth = authenticationManager.authenticate(authReq);

        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);

        return "redirect:/";
    }


    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
}
