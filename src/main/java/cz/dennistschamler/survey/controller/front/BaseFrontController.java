package cz.dennistschamler.survey.controller.front;

import cz.dennistschamler.survey.controller.BaseController;
import cz.dennistschamler.survey.model.Breadcrumb;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseFrontController extends BaseController {
    @ModelAttribute("breadcrumbs")
    public List<Breadcrumb> getBreadcrumbs() {

        List<Breadcrumb> breadcrumbs = new ArrayList<>();

        breadcrumbs.add(new Breadcrumb("Home", "/"));

        return breadcrumbs;
    }
}
