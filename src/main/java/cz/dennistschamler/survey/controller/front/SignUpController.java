package cz.dennistschamler.survey.controller.front;

import cz.dennistschamler.survey.model.Breadcrumb;
import cz.dennistschamler.survey.model.FlashMessage;
import cz.dennistschamler.survey.model.dto.UserSignUpDto;
import cz.dennistschamler.survey.model.entity.user.IUserService;
import cz.dennistschamler.survey.model.entity.user.UserAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class SignUpController extends BaseFrontController {

    private IUserService userService;

    @GetMapping("/sign-up")
    public String signUp(@ModelAttribute("breadcrumbs") List<Breadcrumb> breadcrumbs,  Model model) {
        model.addAttribute("userSignUpDto", new UserSignUpDto());
        breadcrumbs.add(new Breadcrumb("Sign up"));
        return "front/sign-up";
    }

    @PostMapping(value = "/sign-up")
    public String signUpSubmit(@Valid @ModelAttribute("userSignUpDto") UserSignUpDto userSignUpDto, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "front/sign-up";
        }

        try {
            userService.register(userSignUpDto);
        } catch (UserAlreadyExistException ex) {
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("User with [e-mail=<b>"+userSignUpDto.getEmail()+"</b>] already exists", "danger"));
            return "redirect:/sign-up";
        }

        return "redirect:/sign-in";
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
