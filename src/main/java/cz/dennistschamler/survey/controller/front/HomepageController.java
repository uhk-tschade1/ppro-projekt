package cz.dennistschamler.survey.controller.front;

import cz.dennistschamler.survey.model.entity.survey.ISurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomepageController extends BaseFrontController {

    private ISurveyService surveyService;

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("expiringSurveys", surveyService.getSoonToBeExpiredSurveys());
        model.addAttribute("newestSurveys", surveyService.findNewestSurveys());

        return "front/homepage";
    }

    @Autowired
    public void setSurveyService(ISurveyService surveyService) {
        this.surveyService = surveyService;
    }
}
