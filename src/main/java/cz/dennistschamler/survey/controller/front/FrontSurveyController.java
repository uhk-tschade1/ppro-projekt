package cz.dennistschamler.survey.controller.front;

import cz.dennistschamler.survey.model.Breadcrumb;
import cz.dennistschamler.survey.model.FlashMessage;
import cz.dennistschamler.survey.model.dto.SurveyAnswerDto;
import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.model.entity.survey.ISurveyService;
import cz.dennistschamler.survey.model.entity.survey.Survey;
import cz.dennistschamler.survey.model.entity.survey.SurveyAlreadySubmittedException;
import cz.dennistschamler.survey.model.dto.UserResponseDto;
import cz.dennistschamler.survey.model.entity.user.IUserService;
import cz.dennistschamler.survey.model.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/surveys")
public class FrontSurveyController extends BaseFrontController {

    private ISurveyService surveyService;
    private IUserService userService;

    @GetMapping(value={"", "{page}"})
    public String index(@PathVariable(required = false) Integer page, @ModelAttribute("breadcrumbs") List<Breadcrumb> breadcrumbs, Model model) {
        if(page == null) {
            page = 0;
        }

        if(page < 0) {
            return "redirect:/surveys/0";
        }

        int limit = 10;
        long surveyCount = surveyService.countAllActive();
        int lastPage = (int) (Math.ceil(surveyCount/(float)limit))-1;

        if(page > lastPage) {
            return "redirect:/surveys/"+lastPage;
        }

        model.addAttribute("surveys", surveyService.getPaginatedActiveSurveys(PageRequest.of(page, limit)));
        model.addAttribute("page", page);
        model.addAttribute("lastPage", lastPage);

        breadcrumbs.add(new Breadcrumb("Surveys", "/surveys"));

        return "front/survey/index";
    }

    @GetMapping("detail/{id}/{slug}")
    public String detail(@PathVariable int id, @PathVariable String slug, @ModelAttribute("breadcrumbs") List<Breadcrumb> breadcrumbs, Model model, Principal principal, RedirectAttributes redirectAttributes) {

        Survey survey = surveyService.getByIdAndSlug(id, slug);

        if (survey == null) {
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Survey with id=<b>"+id+"</b> and slug=<b>"+slug+"</b> not found", "danger"));
            return "redirect:/surveys";
        }

        User user = userService.getAuthenticatedUser(principal);

        boolean alreadySubmitted = false;

        if(user != null) {
            alreadySubmitted = userService.alreadySubmittedSurvey(survey.getId(), user.getId());
        }

        breadcrumbs.add(new Breadcrumb("Surveys", "/surveys"));
        breadcrumbs.add(new Breadcrumb(survey.getName(), ""));

        SurveyAnswerDto surveyAnswerCreate = new SurveyAnswerDto();

        for(Question q : survey.getQuestions()) {
            surveyAnswerCreate.addUserResponse(new UserResponseDto());
        }

        model.addAttribute("alreadySubmitted", alreadySubmitted);
        model.addAttribute("surveyAnswerDto", surveyAnswerCreate);
        model.addAttribute("survey", survey);
        model.addAttribute("user", user);

        return "front/survey/detail";
    }

    @PostMapping("detail/{id}/{slug}")
    public String detail(@PathVariable int id, @Valid @ModelAttribute("surveyAnswerDto") SurveyAnswerDto surveyAnswerDto, BindingResult bindingResult, Model model, Principal principal, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors().get(0));

            Optional<Survey> surveyOptional = surveyService.findById(id);

            if (surveyOptional.isEmpty()) {
                return "redirect:/surveys";
            }

            Survey survey = surveyOptional.get();

            User user = userService.getAuthenticatedUser(principal);

            boolean alreadySubmitted = false;

            if(user != null) {
                alreadySubmitted = userService.alreadySubmittedSurvey(survey.getId(), user.getId());
            }

            model.addAttribute("alreadySubmitted", alreadySubmitted);
            model.addAttribute("surveyAnswerCreate", surveyAnswerDto);
            model.addAttribute("survey", survey);
            model.addAttribute("user", user);

            return "front/survey/detail";
        }

        Optional<Survey> surveyOptional = surveyService.findById(id);

        if (surveyOptional.isEmpty()) {
            return "redirect:/surveys";
        }

        Survey survey = surveyOptional.get();

        if(survey.hasEnded()) {
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Can´t submit survey with [id=<b>"+id+"</b>]. Survey already ended", "danger"));
            return "redirect:/surveys";
        }

        try {
            surveyService.submitSurvey(surveyAnswerDto);
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Thank you for submitting survey [name=<b>"+survey.getName()+"</b>].", "success"));
        } catch (SurveyAlreadySubmittedException ex) {
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Survey alredy submitted [name=<b>"+survey.getName()+"</b>].", "error"));
        }


        return "redirect:/surveys";
    }


    @Autowired
    public void setSurveyService(ISurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
