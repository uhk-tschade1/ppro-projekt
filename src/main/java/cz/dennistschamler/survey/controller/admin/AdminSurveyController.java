package cz.dennistschamler.survey.controller.admin;

import cz.dennistschamler.survey.model.Action;
import cz.dennistschamler.survey.model.FlashMessage;
import cz.dennistschamler.survey.model.dto.AnswerDto;
import cz.dennistschamler.survey.model.dto.QuestionDto;
import cz.dennistschamler.survey.model.dto.SurveyDto;
import cz.dennistschamler.survey.model.entity.answer.Answer;
import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.model.entity.question.QuestionType;
import cz.dennistschamler.survey.model.entity.survey.ISurveyService;
import cz.dennistschamler.survey.model.entity.survey.Survey;
import cz.dennistschamler.survey.model.entity.user.IUserService;
import cz.dennistschamler.survey.model.entity.user.User;
import cz.dennistschamler.survey.model.entity.user_anwser.IUserAnswerService;
import cz.dennistschamler.survey.model.entity.user_anwser.UserAnswer;
import cz.dennistschamler.survey.model.xlsx.SurveyXlsxView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/admin/surveys")
public class AdminSurveyController extends BaseAdminController {

    private ISurveyService surveyService;
    private IUserAnswerService userAnswerService;
    private IUserService userService;

    @GetMapping(value = {"{page}", ""})
    public String index(@PathVariable(required = false) Integer page, @ModelAttribute("actions") List<Action> actions, Model model) {
        if(page == null)  {
            page = 0;
        }

        if(page < 0) {
            return "redirect:/admin/surveys/0";
        }

        int limit = 10;
        long surveyCount = surveyService.countAll();
        int lastPage = (int) (Math.ceil(surveyCount/(float)limit))-1;

        if(page > lastPage) {
            return "redirect:/admin/surveys/"+lastPage;
        }
        model.addAttribute("surveys", surveyService.getPaginatedSurveys(PageRequest.of(page, limit)));
        model.addAttribute("page", page);
        model.addAttribute("lastPage", lastPage);

        actions.add(new Action("Create", "plus", "/admin/surveys/create"));

        return "admin/survey/index";
    }

    @GetMapping("create")
    public String create(Model model) {
        model.addAttribute("surveyDto", new SurveyDto());
        return "admin/survey/create";
    }

    @PostMapping("create")
    public String createSubmit(@Valid @ModelAttribute("surveyDto") SurveyDto surveyDto, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if(bindingResult.hasErrors()) {
            return "admin/survey/create";
        }

        this.surveyService.create(surveyDto);

        redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Survey <b>"+surveyDto.getName()+"</b> created successfully", "success"));

        return "redirect:/admin/surveys";
    }

    @PostMapping(value = "create", params = {"addQuestion"})
    public String addQuestion(SurveyDto surveyDto) {
        surveyDto.getQuestions().add(new QuestionDto());
        return "admin/survey/create";
    }

    @PostMapping(value = "create", params = {"addAnswer"})
    public String addAnswer(SurveyDto surveyDto, @RequestParam(value = "addAnswer") int questionIndex, Model model) {
        QuestionDto questionDto = surveyDto.getQuestions().get(questionIndex);

        if(questionDto.getType().equals(QuestionType.SINGLE_TEXT) && questionDto.getAnswers().size() == 1) {
            model.addAttribute(FlashMessage.KEY, new FlashMessage("Question with [type=<b>"+QuestionType.SINGLE_TEXT+"</b>] cant have more than one answer", "danger"));
        } else {
            surveyDto.getQuestions().get(questionIndex).addAnswer(new AnswerDto());
        }

        return "admin/survey/create";
    }

    @PostMapping(value = "create", params = {"removeQuestion"})
    public String removeQuestion(SurveyDto surveyDto, @RequestParam(value = "removeQuestion") int questionIndex) {
        surveyDto.getQuestions().remove(questionIndex);
        return "admin/survey/create";
    }

    @PostMapping(value = "create", params = {"removeAnswer"})
    public String removeAnswer(SurveyDto surveyDto, @RequestParam(value = "removeAnswer") String questionAnswerIndex) {
        String[] s = questionAnswerIndex.split("-");
        int questionIndex = Integer.parseInt(s[0]);
        int answerIndex = Integer.parseInt(s[1]);

        surveyDto.getQuestions().get(questionIndex).getAnswers().remove(answerIndex);
        return "admin/survey/create";
    }

    @GetMapping(value = "edit/{id}")
    public String edit(@PathVariable int id, Model model, @ModelAttribute("actions") List<Action> actions, RedirectAttributes redirectAttributes) {
        Optional<Survey> surveyOptional = surveyService.findById(id);

        if(surveyOptional.isEmpty()) {
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Survey with [id=<b>"+id+"</b>] not found", "danger"));
            return "redirect:/admin/surveys";
        }

        Survey survey = surveyOptional.get();

        SurveyDto surveyEdit = new SurveyDto();

        surveyEdit.setId(survey.getId());
        surveyEdit.setName(survey.getName());
        surveyEdit.setDescription(survey.getDescription());
        surveyEdit.setEndsAt(survey.getEndsAt());

        for(Question q : survey.getQuestions()) {
            QuestionDto questionDto = new QuestionDto();
            questionDto.setId(q.getId());
            questionDto.setLabel(q.getLabel());
            questionDto.setType(q.getType());
            questionDto.setSort(q.getSort());

            for(Answer a : q.getAnswers()) {
                AnswerDto answerDto = new AnswerDto();

                answerDto.setId(a.getId());
                answerDto.setLabel(a.getLabel());

                questionDto.addAnswer(answerDto);
            }

            surveyEdit.addQuestion(questionDto);
        }

        actions.add(new Action("Back", "chevron-left", "/admin/surveys"));
        model.addAttribute("surveyDto", surveyEdit);

        return "admin/survey/edit";
    }

    @PostMapping(value = "edit/{id}")
    public String editSubmit(@PathVariable("id") int id, @Valid @ModelAttribute("surveyDto") SurveyDto surveyDto, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

        if(bindingResult.hasErrors()) {

            Optional<Survey> surveyOptional = surveyService.findById(id);

            if(surveyOptional.isEmpty()) {
                redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Survey with [id=<b>"+id+"</b>] not found", "danger"));
                return "redirect:/admin/surveys";
            }

            Survey s = surveyOptional.get();

            model.addAttribute("surveyDto", surveyDto);

            return "admin/survey/edit";
        }

        surveyService.edit(surveyDto);

        redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Survey <b>"+surveyDto.getName()+"</b> updated successfully", "success"));

        return "redirect:/admin/surveys";
    }


    @PostMapping(value = "edit/{id}", params = {"addQuestion"})
    public String addQuestion(@PathVariable("id") int id, @ModelAttribute("surveyDto") SurveyDto surveyDto, Model model) {
        surveyDto.getQuestions().add(new QuestionDto());
        return "admin/survey/edit";
    }

    @PostMapping(value = "edit/{id}", params = {"addAnswer"})
    public String addAnswer(@PathVariable("id") int id,  @ModelAttribute("surveyDto") SurveyDto surveyDto, @RequestParam(value = "addAnswer") int questionIndex, Model model) {
        QuestionDto questionDto = surveyDto.getQuestions().get(questionIndex);

        if(questionDto.getType().equals(QuestionType.SINGLE_TEXT) && questionDto.getAnswers().size() == 1) {
            model.addAttribute(FlashMessage.KEY, new FlashMessage("Question with [type=<b>"+QuestionType.SINGLE_TEXT+"</b>] cant have more than one answer", "danger"));
        } else {
            surveyDto.getQuestions().get(questionIndex).addAnswer(new AnswerDto());
        }

        return "admin/survey/edit";
    }

    @PostMapping(value = "edit/{id}", params = {"removeQuestion"})
    public String removeQuestion(@PathVariable("id") int id,  @ModelAttribute("surveyDto") SurveyDto surveyDto, @RequestParam(value = "removeQuestion") int questionIndex) {
        surveyDto.getQuestions().remove(questionIndex);
        return "admin/survey/edit";
    }

    @PostMapping(value = "edit/{id}", params = {"removeAnswer"})
    public String removeAnswer(@PathVariable("id") int id,  @ModelAttribute("surveyDto") SurveyDto surveyDto, @RequestParam(value = "removeAnswer") String questionAnswerIndex) {
        String[] s = questionAnswerIndex.split("-");
        int questionIndex = Integer.parseInt(s[0]);
        int answerIndex = Integer.parseInt(s[1]);

        surveyDto.getQuestions().get(questionIndex).getAnswers().remove(answerIndex);
        return "admin/survey/edit";
    }

    @PostMapping(value = "delete/{id}")
    public String delete(@PathVariable int id, RedirectAttributes redirectAttrs) {
        surveyService.delete(id);

        return "redirect:/admin/surveys";

    }

    @GetMapping(value = "detail/{id}")
    public String detail(@ModelAttribute("actions") List<Action> actions, @PathVariable int id, Model model, RedirectAttributes redirectAttributes) {
        Optional<Survey> surveyOptional = this.surveyService.findById(id);

        if(surveyOptional.isEmpty()) {
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Survey with [id=<b>"+id+"</b>] not found", "danger"));
            return "redirect:/admin/surveys";
        }

        Survey survey = surveyOptional.get();

        List<User> users = userService.getUsersBySurvey(survey.getId());

        actions.add(new Action("Back", "chevron-left", "/admin/surveys"));
        actions.add(new Action("Edit", "pencil-alt", "/admin/surveys/edit/"+id, "primary"));
        actions.add(new Action("Export", "file-export", "/admin/surveys/export/"+id+"/survey.xlsx", "danger"));

        model.addAttribute("survey", survey);
        model.addAttribute("users", users);

        return "admin/survey/detail";
    }

    @GetMapping(value = "detail/{id}/response/{userId}")
    public String detailResponse(@PathVariable int id, @PathVariable int userId, @ModelAttribute("actions") List<Action> actions, Model model, RedirectAttributes redirectAttributes) {
        Optional<Survey> survey = this.surveyService.findById(id);
        Optional<User> user = userService.findById(userId);

        if(survey.isEmpty() || user.isEmpty()) {
            redirectAttributes.addFlashAttribute(FlashMessage.KEY, new FlashMessage("Responses for survey with [id=<b>"+id+"</b>] and user with id=<b>"+userId+"</b> not found", "danger"));
            return "redirect:/admin/surveys/detail/"+id;
        }

        Map<Question, List<UserAnswer>> map = new LinkedHashMap<>();

        for(Question q : survey.get().getQuestions()) {
            List<UserAnswer> userAnswers = userAnswerService.findUserAnswersByUserAndQuestion(userId, q.getId());
            map.put(q, userAnswers);
        }

        actions.add(new Action("Back", "chevron-left", "/admin/surveys/detail/"+id));

        model.addAttribute("user", user.get());
        model.addAttribute("survey", survey.get());
        model.addAttribute("map", map);

        return "admin/survey/response-detail";
    }

    @GetMapping("/export/{id}/survey.xlsx")
    public View export(@PathVariable int id, Map<String, Object> model) {
        Optional<Survey> surveyOptional = surveyService.findById(id);

        if(surveyOptional.isEmpty()) {
            return null;
        }

        Survey survey = surveyOptional.get();

        model.put("survey", survey);
        return new SurveyXlsxView();
    }

    @Autowired
    public void setSurveyService(ISurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Autowired
    public void setUserAnswerService(IUserAnswerService userAnswerService) {
        this.userAnswerService = userAnswerService;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
