package cz.dennistschamler.survey.controller.admin;

import cz.dennistschamler.survey.model.entity.survey.ISurveyService;
import cz.dennistschamler.survey.model.entity.user.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class DashboardController extends BaseAdminController {

    private ISurveyService surveyService;
    private IUserService userService;

    @GetMapping("/admin")
    public String index(Model model, Principal principal) {

        model.addAttribute("user", userService.getAuthenticatedUser(principal));
        model.addAttribute("userCount", userService.countAll());
        model.addAttribute("surveyCount", surveyService.countAll());
        model.addAttribute("newestSurveys", surveyService.findNewestSurveys());
        model.addAttribute("newestUsers", userService.findFirst10ByOrderByCreatedAtDesc());

        return "admin/dashboard/index";
    }

    @Autowired
    public void setSurveyService(ISurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
