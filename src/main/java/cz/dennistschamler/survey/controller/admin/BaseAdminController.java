package cz.dennistschamler.survey.controller.admin;

import cz.dennistschamler.survey.controller.BaseController;
import cz.dennistschamler.survey.model.Action;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdminController extends BaseController {
    @ModelAttribute("actions")
    public List<Action> getActions() {
        return new ArrayList<>();
    }
}
