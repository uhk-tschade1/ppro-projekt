package cz.dennistschamler.survey.controller.admin;

import cz.dennistschamler.survey.model.Action;
import cz.dennistschamler.survey.model.FlashMessage;
import cz.dennistschamler.survey.model.entity.user.IUserService;
import cz.dennistschamler.survey.model.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin/users")
public class UserController extends BaseAdminController {

    private IUserService userService;

    @GetMapping(value = {"", "{page}"})
    public String index(@PathVariable(required = false) Integer page, @ModelAttribute("actions") List<Action> actions, Model model) {
        if(page == null)  {
            page = 0;
        }

        if(page < 0) {
            return "redirect:/admin/users/0";
        }

        int limit = 10;
        long userCount = userService.countAll();
        int lastPage = (int) (Math.ceil(userCount/(float)limit))-1;

        if(page > lastPage) {
            return "redirect:/admin/users/"+lastPage;
        }

        model.addAttribute("users", userService.getPaginatedUsers(PageRequest.of(page, limit)));
        model.addAttribute("page", page);
        model.addAttribute("lastPage", lastPage);

        return "admin/user/index";
    }

    @GetMapping("create")
    public String create() {
        return "admin/user/create";
    }

    @PostMapping("delete/{id}")
    public String delete(@PathVariable int id, RedirectAttributes redirectAttrs) {

        if(!userService.canDeleteUser(id)) {
            redirectAttrs.addFlashAttribute(FlashMessage.KEY, new FlashMessage("User with [id=<b>"+id+"</b>] can´t be deleted", "danger"));
            return "redirect:/admin/users";
        }

        userService.delete(id);

        redirectAttrs.addFlashAttribute(FlashMessage.KEY, new FlashMessage("User with [id=<b>"+id+"</b>] deleted", "success"));

        return "redirect:/admin/users";
    }

    @GetMapping("detail/{id}")
    public String detail(@PathVariable int id, @ModelAttribute("actions") List<Action> actions, Model model) {

        Optional<User> userOptional = userService.findById(id);

        if(userOptional.isEmpty()) {
            // THROW ERROR AND REDIRECT
            return "redirect:/admin/users";
        }

        User user = userOptional.get();

        actions.add(new Action("Back", "chevron-left", "/admin/users"));
        model.addAttribute("user", user);

        return "admin/user/detail";
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
