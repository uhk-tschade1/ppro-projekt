package cz.dennistschamler.survey.api;

import cz.dennistschamler.survey.model.dto.chart.DataSet;
import cz.dennistschamler.survey.model.dto.chart.PieChart;
import cz.dennistschamler.survey.model.dto.chart.PieChartData;
import cz.dennistschamler.survey.model.entity.answer.Answer;
import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.model.entity.question.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/chart")
public class ChartController {

    private QuestionService questionService;

    @Autowired
    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping(value = "question-pie/{id}")
    public ResponseEntity<PieChart> questionPie(@PathVariable int id) {
        Optional<Question> questionOptional = this.questionService.findById(id);

        if (questionOptional.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        Question question = questionOptional.get();

        PieChart pieChart = new PieChart();

        PieChartData pieChartData = new PieChartData();

        DataSet dataSet = new DataSet();
        for(Answer a : question.getAnswers()) {
            if(a.getUserAnswers().size() == 0) {
                continue;
            }

            dataSet.addDataPoint(a.getUserAnswers().size(), a.getLabel());
        }

        if(!dataSet.getData().isEmpty()) {
            pieChartData.addDataSet(dataSet);
        }

        pieChart.setData(pieChartData);


        return new ResponseEntity<>(pieChart, HttpStatus.OK);
    }

}
