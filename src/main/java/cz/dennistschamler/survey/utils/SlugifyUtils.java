package cz.dennistschamler.survey.utils;

import com.github.slugify.Slugify;

public class SlugifyUtils {

    private static Slugify slugify;

    static {
        slugify = new Slugify();
    }

    public static String slugify(String str) {
        return slugify.slugify(str);
    }
}
