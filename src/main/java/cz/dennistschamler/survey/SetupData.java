package cz.dennistschamler.survey;

import com.github.javafaker.Faker;
import cz.dennistschamler.survey.model.entity.answer.Answer;
import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.model.entity.question.QuestionType;
import cz.dennistschamler.survey.model.entity.role.IRoleService;
import cz.dennistschamler.survey.model.entity.role.Role;
import cz.dennistschamler.survey.model.entity.survey.ISurveyService;
import cz.dennistschamler.survey.model.entity.survey.Survey;
import cz.dennistschamler.survey.model.entity.user.IUserService;
import cz.dennistschamler.survey.model.entity.user.User;
import cz.dennistschamler.survey.model.entity.user_anwser.IUserAnswerService;
import cz.dennistschamler.survey.model.entity.user_anwser.UserAnswer;
import cz.dennistschamler.survey.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class SetupData implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    private PasswordEncoder passwordEncoder;
    private IUserService userService;
    private ISurveyService surveyService;
    private IRoleService roleService;
    private IUserAnswerService userAnswerService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (alreadySetup)
            return;

        createRoleIfNotFound("ROLE_ADMIN");
        createRoleIfNotFound("ROLE_MEMBER");

        Role adminRole = roleService.findByName("ROLE_ADMIN");

        User adminUser = new User();

        adminUser.setEmail("admin@admin.cz");
        adminUser.setPassword(passwordEncoder.encode("admin"));
        adminUser.setFirstName("Josef");
        adminUser.setLastName("Novák");
        adminUser.setRole(adminRole);

        userService.save(adminUser);

        Role memberRole = roleService.findByName("ROLE_MEMBER");

        User memberUser = new User();
        memberUser.setEmail("member@member.cz");
        memberUser.setPassword(passwordEncoder.encode("member"));
        memberUser.setFirstName("John");
        memberUser.setLastName("Hill");
        memberUser.setRole(memberRole);

        userService.save(memberUser);

        Faker faker = new Faker();

        // GENERATE USERS
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            User user = new User();

            user.setEmail(faker.internet().safeEmailAddress());
            user.setPassword(passwordEncoder.encode("test"));
            user.setFirstName(faker.name().firstName());
            user.setLastName(faker.name().lastName());
            user.setRole(memberRole);

            users.add(user);
        }

        userService.saveAll(users);

        long aDay = TimeUnit.DAYS.toMillis(1);
        long now = new Date().getTime();

        // GENERATE SURVEYS
        List<Survey> surveys = new ArrayList<>();
        for (int i = 0; i < 33; i++) {
            Survey survey = new Survey();

            survey.setName(faker.book().title());
            survey.setDescription(String.join(" ", faker.lorem().sentences((int)(Math.random() * 10))));
            survey.setEndsAt(DateUtils.getRandomDateBetween(new Date(now - aDay * 10), new Date(now + aDay * 10)));

            for (int j = 0; j < (int) (Math.floor(Math.random() * 10) + 1); j++) {
                Question q1 = new Question();
                q1.setSort(j);
                q1.setLabel(faker.book().title());

                int numOfAnswers = 1;

                double r = Math.random();

                if (r < 0.3) {
                    q1.setType(QuestionType.MULTIPLE_CHOICE);
                    numOfAnswers = (int) (Math.floor(Math.random() * 5) + 2);
                } else if (r < 0.6) {
                    q1.setType(QuestionType.SINGLE_CHOICE);
                    numOfAnswers = (int) (Math.floor(Math.random() * 5) + 2);
                }

                for (int k = 0; k < numOfAnswers; k++) {
                    Answer a = new Answer();
                    a.setSort(k);
                    a.setLabel(faker.harryPotter().character());
                    q1.addAnswer(a);
                }

                survey.addQuestion(q1);
            }

            surveys.add(survey);
        }

        surveyService.saveAll(surveys);

        List<UserAnswer> userAnswers = new ArrayList<>();
        // GENERATE USER ANSWERS
        for(Survey s : surveys) {
            for(User u : users) {
                for(Question q : s.getQuestions()) {

                    for(Answer a : q.getAnswers()) {
                        double r = Math.random();

                        if(r > 0.5 || q.getType().equals(QuestionType.SINGLE_TEXT)) {
                            UserAnswer userAnswer = new UserAnswer();

                            userAnswer.setUser(u);
                            userAnswer.setResponse(q.getType() == QuestionType.SINGLE_TEXT ? faker.lorem().sentence() : a.getLabel());
                            userAnswer.setAnswer(a);

                            userAnswers.add(userAnswer);
                        }
                    }
                }
            }
        }

        userAnswerService.saveAll(userAnswers);

        alreadySetup = true;
    }

    @Transactional
    Role createRoleIfNotFound(
            String name) {
        Role role = roleService.findByName(name);
        if (role == null) {
            role = new Role();
            role.setName(name);
            roleService.save(role);
        }
        return role;
    }


    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setSurveyService(ISurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Autowired
    public void setRoleService(IRoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setUserAnswerService(IUserAnswerService userAnswerService) {
        this.userAnswerService = userAnswerService;
    }
}
