package cz.dennistschamler.survey.model.sitemap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "urlset", namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")
public class Sitemap {

    private String baseUrl;

    @XmlElements(@XmlElement(name = "url", type = Url.class))
    private List<Url> urls = new ArrayList<>();

    public Sitemap() {

    }

    public Sitemap(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Url addUrl(String url) {
        Url u= new Url();
        u.setUrl(baseUrl + "/" + url);
        urls.add(u);
        return u;
    }

    public static class Url {


        private String url;

        private Date lastModified = new Date();

        private ChangeFrequency changeFrequency = ChangeFrequency.ALWAYS;

        private float priority = 1.0f;

        public Url() {
        }

        @XmlElement(required = true, name = "loc")
        public Url setUrl(String url) {
            this.url = url;
            return this;
        }

        public String getUrl() {
            return url;
        }

        public Date getLastModified() {
            return lastModified;
        }

        @XmlElement(name = "lastmod")
        public Url setLastModified(Date lastModified) {
            this.lastModified = lastModified;
            return this;
        }

        public ChangeFrequency getChangeFrequency() {
            return changeFrequency;
        }

        @XmlElement(name = "changefreq")
        public Url setChangeFrequency(ChangeFrequency changeFrequency) {
            this.changeFrequency = changeFrequency;
            return this;
        }

        public float getPriority() {
            return priority;
        }

        @XmlElement
        public Url setPriority(float priority) {
            this.priority = priority;
            return this;
        }
    }

    @XmlEnum
    public enum ChangeFrequency {

        @XmlEnumValue("always")
        ALWAYS("always"),
        @XmlEnumValue("hourly")
        HOURLY("hourly"),
        @XmlEnumValue("daily")
        DAILY("daily"),
        @XmlEnumValue("weekly")
        WEEKLY("weeekly"),
        @XmlEnumValue("monthly")
        MONTHLY("monthly"),
        @XmlEnumValue("yearly")
        YEARLY("yearly"),
        @XmlEnumValue("never")
        NEVER("never"),;



        private final String value;

        ChangeFrequency(String value) {
            this.value = value;
        }
    }

}
