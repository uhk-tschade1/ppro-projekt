package cz.dennistschamler.survey.model.entity.survey;

import cz.dennistschamler.survey.model.entity.BaseEntity;
import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.utils.DateUtils;
import cz.dennistschamler.survey.utils.SlugifyUtils;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="surveys")
public class Survey extends BaseEntity {

    @Id
    @GeneratedValue()
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String slug;

    @Column(nullable = false)
    private Date endsAt;

    @Column(columnDefinition="TEXT", nullable = false)
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "survey", fetch = FetchType.EAGER, orphanRemoval = true)
    @OrderBy("sort asc")
    private Set<Question> questions = new HashSet<>();

    public Survey() {

    }
    
    public boolean hasEnded() {
        Date now = DateUtils.getCurrentDateWithoutTime();
        return endsAt.before(now);
    }

    public void addQuestion(Question question) {
        question.setSurvey(this);
        this.questions.add(question);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.slug = SlugifyUtils.slugify(name);
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public String getSlug() {
        return slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEndsAt() {
        return endsAt;
    }

    public void setEndsAt(Date endsAt) {
        this.endsAt = endsAt;
    }
}
