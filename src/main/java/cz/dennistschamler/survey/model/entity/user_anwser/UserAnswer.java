package cz.dennistschamler.survey.model.entity.user_anwser;

import cz.dennistschamler.survey.model.entity.BaseEntity;
import cz.dennistschamler.survey.model.entity.answer.Answer;
import cz.dennistschamler.survey.model.entity.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_answers")
public class UserAnswer extends BaseEntity {

    @Id
    @GeneratedValue()
    private int id;

    @Column(nullable = false)
    private String response;

    @ManyToOne
    private User user;

    @ManyToOne
    private Answer answer;

    public UserAnswer() {

    }

    public int getId() {
        return id;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }
}
