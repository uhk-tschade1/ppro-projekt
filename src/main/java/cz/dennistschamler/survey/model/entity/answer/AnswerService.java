package cz.dennistschamler.survey.model.entity.answer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerService implements IAnswerService{

    private AnswerRepository answerRepository;

    @Override
    public long countAll() {
        return answerRepository.count();
    }

    @Override
    public Optional<Answer> findById(Integer id) {
        return answerRepository.findById(id);
    }

    @Override
    public Answer getOne(Integer id) {
        return answerRepository.getOne(id);
    }

    @Override
    public List<Answer> findAll() {
        return answerRepository.findAll();
    }

    @Override
    public void save(Answer entity) {
        answerRepository.save(entity);
    }

    @Override
    public void saveAll(Iterable<Answer> entities) {
        answerRepository.saveAll(entities);
    }

    @Override
    public void delete(Integer id) {
        answerRepository.deleteById(id);
    }

    @Autowired
    public void setAnswerRepository(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }
}
