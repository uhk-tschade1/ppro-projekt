package cz.dennistschamler.survey.model.entity.question;

import cz.dennistschamler.survey.model.entity.answer.Answer;
import cz.dennistschamler.survey.model.entity.survey.Survey;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="questions")
public class Question {

    @Id
    @GeneratedValue()
    private int id;

    @Column(nullable = false)
    private int sort;

    @Column(nullable = false)
    private String label;

    @Column(nullable = false)
    private QuestionType type = QuestionType.SINGLE_TEXT;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Survey survey;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question", fetch = FetchType.EAGER, orphanRemoval=true)
    @OrderBy("sort asc")
    private Set<Answer> answers = new HashSet<>();

    public Question() {

    }

    public void addAnswer(Answer answer) {
        answer.setQuestion(this);
        this.answers.add(answer);
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType questionType) {
        this.type = questionType;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int order) {
        this.sort = order;
    }
}
