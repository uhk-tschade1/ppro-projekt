package cz.dennistschamler.survey.model.entity.survey;

import cz.dennistschamler.survey.model.dto.AnswerDto;
import cz.dennistschamler.survey.model.dto.QuestionDto;
import cz.dennistschamler.survey.model.dto.SurveyAnswerDto;
import cz.dennistschamler.survey.model.dto.SurveyDto;
import cz.dennistschamler.survey.model.dto.UserResponseDto;
import cz.dennistschamler.survey.model.entity.answer.Answer;
import cz.dennistschamler.survey.model.entity.answer.IAnswerService;
import cz.dennistschamler.survey.model.entity.question.IQuestionService;
import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.model.entity.user.IUserService;
import cz.dennistschamler.survey.model.entity.user_anwser.IUserAnswerService;
import cz.dennistschamler.survey.model.entity.user_anwser.UserAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SurveyService implements ISurveyService {

    private SurveyRepository surveyRepository;
    private IUserAnswerService userAnswerService;
    private IAnswerService answerService;
    private IUserService userService;
    private IQuestionService questionService;

    public SurveyService() {
    }

    @Override
    public List<Survey> findNewestSurveys() {
        return surveyRepository.findFirst10ByOrderByCreatedAtDesc();
    }

    @Override
    public long countAll() {
        return surveyRepository.count();
    }

    @Override
    public Optional<Survey> findById(Integer id) {
        return surveyRepository.findById(id);
    }

    @Override
    public Survey getOne(Integer id) {
        return surveyRepository.getOne(id);
    }

    @Override
    public List<Survey> findAll() {
        return surveyRepository.findAll();
    }

    public Survey getByIdAndSlug(Integer id, String slug) {
        return surveyRepository.getByIdAndSlug(id, slug);
    }

    @Override
    public long countAllActive() {
        return surveyRepository.countAllActive();
    }

    @Override
    public List<Survey> getPaginatedSurveys(Pageable pageable) {
        return surveyRepository.findAll(pageable).getContent();
    }

    @Override
    public List<Survey> getPaginatedActiveSurveys(Pageable pageable) {
        return surveyRepository.findActiveSurveys(pageable).getContent();
    }

    @Override
    public List<Survey> getSoonToBeExpiredSurveys() {
        return surveyRepository.findSurveysSoonToBeExpired(PageRequest.of(0, 10));
    }

    @Override
    public Survey findLatestSurvey() {
        return surveyRepository.findFirstByOrderByCreatedAtDesc();
    }

    @Override
    public void delete(Integer id) {
        surveyRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void create(SurveyDto surveyDto) {
        Survey survey = new Survey();

        survey.setName(surveyDto.getName());
        survey.setDescription(surveyDto.getDescription());
        survey.setEndsAt(surveyDto.getEndsAt());

        for (QuestionDto questionDto : surveyDto.getQuestions()) {
            Question question = new Question();
            question.setLabel(questionDto.getLabel());
            question.setSurvey(survey);
            question.setType(questionDto.getType());
            question.setSort(questionDto.getSort());

            for (int i = 0; i < questionDto.getAnswers().size(); i++) {
                Answer answer = new Answer();

                answer.setSort(i);
                answer.setLabel(questionDto.getAnswers().get(i).getLabel());
                answer.setQuestion(question);

                question.addAnswer(answer);
            }

            survey.addQuestion(question);
        }

        save(survey);
    }

    @Override
    public void save(Survey survey) {
        this.surveyRepository.save(survey);
    }

    @Override
    public void saveAll(Iterable<Survey> surveys) {
        surveyRepository.saveAll(surveys);
    }

    @Override
    @Transactional
    public void edit(SurveyDto surveyDto) {
        Optional<Survey> surveyOptional = findById(surveyDto.getId());

        if (surveyOptional.isEmpty()) {
            return;
        }

        Survey survey = surveyOptional.get();

        survey.setName(surveyDto.getName());
        survey.setDescription(surveyDto.getDescription());
        survey.setEndsAt(surveyDto.getEndsAt());

        // REMOVE QUESTIONS AND ANSWERS
        List<Integer> questionIdsToRemove = new ArrayList<>();
        for (Question q : survey.getQuestions()) {

            int questionId = q.getId();

            boolean foundQuestion = false;
            for (QuestionDto questionDto : surveyDto.getQuestions()) {
                if (questionDto.getId().equals(questionId)) {
                    foundQuestion = true;
                    break;
                }
            }

            if (!foundQuestion) {
                questionIdsToRemove.add(questionId);
                continue;
            }

            QuestionDto questionDto = findQuestion(surveyDto.getQuestions(), q.getId());

            if(questionDto == null) {
                continue;
            }

            List<Integer> answerIdsToRemove = new ArrayList<>();

            for (Answer a : q.getAnswers()) {
                int answerId = a.getId();

                boolean foundAnswer = false;
                for (AnswerDto answerDto : questionDto.getAnswers()) {
                    if (answerDto.getId().equals(answerId)) {
                        foundAnswer = true;
                        break;
                    }
                }

                if (!foundAnswer) {
                    answerIdsToRemove.add(answerId);
                }
            }

            q.getAnswers().removeIf(a -> answerIdsToRemove.contains(a.getId()));
        }

        survey.getQuestions().removeIf(q -> questionIdsToRemove.contains(q.getId()));

        // CREATE/UPDATE QUESTIONS AND ANSWERS
        for (QuestionDto questionDto : surveyDto.getQuestions()) {
            // If question has id, update
            // If question no id, create

            Question q = new Question();
            if (questionDto.getId() != null) {
                q = questionService.getOne(questionDto.getId());
            }

            q.setLabel(questionDto.getLabel());
            q.setSort(questionDto.getSort());
            q.setSurvey(survey);

            if (questionDto.getId() == null) {
                survey.addQuestion(q);
            }

            for (AnswerDto answerDto : questionDto.getAnswers()) {

                // If answer has id, update
                // If answer no id, create

                Answer a = new Answer();

                if (answerDto.getId() != null) {
                    a = answerService.getOne(answerDto.getId());
                }

                a.setLabel(answerDto.getLabel());
                a.setQuestion(q);
                a.setSort(answerDto.getSort());

                if (answerDto.getId() == null) {
                    q.addAnswer(a);
                }

            }
        }


        this.surveyRepository.save(survey);
    }

    private QuestionDto findQuestion(List<QuestionDto> questions, int id) {
        for (QuestionDto questionDto : questions) {
            if (questionDto.getId() == id) {
                return questionDto;
            }
        }
        return null;
    }

    @Transactional
    @Override
    public void submitSurvey(SurveyAnswerDto surveyAnswerCreate) throws SurveyAlreadySubmittedException {

        int userId = surveyAnswerCreate.getUserId();
        int surveyId = surveyAnswerCreate.getSurveyId();

        if(userService.alreadySubmittedSurvey(surveyId, userId)) {
            throw new SurveyAlreadySubmittedException("Survey id=\""+surveyId+"\" already submitted by user id=\""+userId+"\"");
        }

        for (UserResponseDto userResponse : surveyAnswerCreate.getUserResponses()) {

            // SINGLE ANSWER
            if (userResponse.getAnswers().size() == 1) {
                UserAnswer userAnswer = new UserAnswer();

                userAnswer.setAnswer(answerService.getOne(userResponse.getAnswers().get(0)));
                userAnswer.setResponse(userResponse.getResponses().get(0));
                userAnswer.setUser(userService.getOne(userId));

                this.userAnswerService.save(userAnswer);

                continue;
            }

            for (int answerId : userResponse.getAnswers()) {
                Answer answer = answerService.getOne(answerId);
                for (String response : userResponse.getResponses()) {
                    if (answer.getLabel().equals(response)) {
                        UserAnswer userAnswer = new UserAnswer();

                        userAnswer.setAnswer(answer);
                        userAnswer.setResponse(response);
                        userAnswer.setUser(userService.getOne(userId));

                        this.userAnswerService.save(userAnswer);
                        break;
                    }
                }
            }
        }
    }

    @Autowired
    public void setSurveyRepository(SurveyRepository surveyRepository) {
        this.surveyRepository = surveyRepository;
    }

    @Autowired
    public void setUserAnswerService(IUserAnswerService userAnswerService) {
        this.userAnswerService = userAnswerService;
    }

    @Autowired
    public void setAnswerService(IAnswerService answerService) {
        this.answerService = answerService;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setQuestionService(IQuestionService questionService) {
        this.questionService = questionService;
    }
}
