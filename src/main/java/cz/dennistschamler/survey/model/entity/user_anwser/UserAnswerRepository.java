package cz.dennistschamler.survey.model.entity.user_anwser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAnswerRepository extends JpaRepository<UserAnswer, Integer> {
    @Query("SELECT ua FROM UserAnswer ua WHERE ua.user.id = ?1 AND ua.answer.question.id = ?2")
    List<UserAnswer> findUserAnswersByUserAndQuestion(int userId, int questionId);
}
