package cz.dennistschamler.survey.model.entity.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findOneByEmail(String email);

    @Query("SELECT DISTINCT u FROM User u LEFT JOIN u.userAnswers ua WHERE ua.answer.question.survey.id = ?1")
    List<User> findUsersBySurvey(int surveyId);

    @Query("SELECT case when (count(u) > 0) then true else false end FROM User u LEFT JOIN u.userAnswers ua WHERE ua.answer.question.survey.id = ?1 AND u.id = ?2")
    boolean alreadySubmittedSurvey(int surveyId, int userId);


    User findFirstByOrderByCreatedAtDesc();

    List<User> findFirst10ByOrderByCreatedAtDesc();
}
