package cz.dennistschamler.survey.model.entity.survey;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyRepository extends JpaRepository<Survey, Integer> {
    List<Survey> findFirst10ByOrderByCreatedAtDesc();

    @Query("SELECT DISTINCT s FROM Survey s WHERE s.endsAt >= CURRENT_DATE ORDER BY s.endsAt ASC")
    List<Survey> findSurveysSoonToBeExpired(Pageable pageable);

    Survey getByIdAndSlug(int id, String slug);

    Survey findFirstByOrderByCreatedAtDesc();

    @Query("SELECT DISTINCT COUNT(s) FROM Survey s WHERE s.endsAt >= CURRENT_DATE")
    long countAllActive();

    @Query("SELECT DISTINCT s FROM Survey s WHERE s.endsAt >= CURRENT_DATE ORDER BY s.endsAt ASC")
    Page<Survey> findActiveSurveys(Pageable pageable);
}
