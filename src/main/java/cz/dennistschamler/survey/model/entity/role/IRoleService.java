package cz.dennistschamler.survey.model.entity.role;

import cz.dennistschamler.survey.model.entity.BaseService;

public interface IRoleService extends BaseService<Role, Integer> {
    Role findByName(String name);
}
