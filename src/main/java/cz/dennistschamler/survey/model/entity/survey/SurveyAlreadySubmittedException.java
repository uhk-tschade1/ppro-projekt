package cz.dennistschamler.survey.model.entity.survey;

public class SurveyAlreadySubmittedException extends Exception  {

    SurveyAlreadySubmittedException(String message) {
        super(message);
    }
}
