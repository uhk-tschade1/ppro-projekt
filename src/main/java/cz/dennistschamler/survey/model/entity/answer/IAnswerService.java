package cz.dennistschamler.survey.model.entity.answer;

import cz.dennistschamler.survey.model.entity.BaseService;

public interface IAnswerService extends BaseService<Answer, Integer> {
}
