package cz.dennistschamler.survey.model.entity.question;

import cz.dennistschamler.survey.model.entity.BaseService;

public interface IQuestionService extends BaseService<Question, Integer> {
}
