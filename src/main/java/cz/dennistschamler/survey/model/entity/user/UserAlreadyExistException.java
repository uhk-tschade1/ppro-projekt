package cz.dennistschamler.survey.model.entity.user;

public class UserAlreadyExistException extends Exception  {

    UserAlreadyExistException(String message) {
        super(message);
    }
}
