package cz.dennistschamler.survey.model.entity.user_anwser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserAnswerService implements IUserAnswerService {
    private UserAnswerRepository userAnswerRepository;

    public List<UserAnswer> findUserAnswersByUserAndQuestion(int userId, int questionId) {
        return userAnswerRepository.findUserAnswersByUserAndQuestion(userId, questionId);
    }

    @Override
    public void save(UserAnswer userAnswer) {
        userAnswerRepository.save(userAnswer);
    }

    @Override
    public void saveAll(Iterable<UserAnswer> userAnswers) {
        userAnswerRepository.saveAll(userAnswers);
    }

    @Override
    public void delete(Integer id) {
        userAnswerRepository.deleteById(id);
    }

    @Override
    public long countAll() {
        return userAnswerRepository.count();
    }

    @Override
    public Optional<UserAnswer> findById(Integer id) {
        return userAnswerRepository.findById(id);
    }

    @Override
    public UserAnswer getOne(Integer id) {
        return userAnswerRepository.getOne(id);
    }

    @Override
    public List<UserAnswer> findAll() {
        return userAnswerRepository.findAll();
    }

    @Autowired
    public void setUserAnswerRepository(UserAnswerRepository userAnswerRepository) {
        this.userAnswerRepository = userAnswerRepository;
    }
}
