package cz.dennistschamler.survey.model.entity.user_anwser;

import cz.dennistschamler.survey.model.entity.BaseService;

import java.util.List;

public interface IUserAnswerService extends BaseService<UserAnswer, Integer> {
    List<UserAnswer> findUserAnswersByUserAndQuestion(int userId, int questionId);
}
