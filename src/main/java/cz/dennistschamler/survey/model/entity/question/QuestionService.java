package cz.dennistschamler.survey.model.entity.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionService implements IQuestionService {

    private QuestionRepository questionRepository;

    public QuestionService() {
    }

    @Override
    public long countAll() {
        return questionRepository.count();
    }

    @Override
    public Optional<Question> findById(Integer id) {
        return questionRepository.findById(id);
    }

    @Override
    public Question getOne(Integer id) {
        return questionRepository.getOne(id);
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public void save(Question entity) {
        questionRepository.save(entity);
    }

    @Override
    public void saveAll(Iterable<Question> entities) {
        questionRepository.saveAll(entities);
    }

    @Override
    public void delete(Integer id) {
        questionRepository.deleteById(id);
    }

    @Autowired
    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }
}
