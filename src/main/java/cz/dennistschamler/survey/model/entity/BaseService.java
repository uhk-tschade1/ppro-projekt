package cz.dennistschamler.survey.model.entity;

import java.util.List;
import java.util.Optional;

public interface BaseService<T, U> {
    long countAll();
    Optional<T> findById(U id);
    T getOne(U id);
    List<T> findAll();
    void save(T entity);
    void saveAll(Iterable<T> entities);
    void delete(U id);
}
