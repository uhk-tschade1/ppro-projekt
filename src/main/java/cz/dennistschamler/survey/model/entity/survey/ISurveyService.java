package cz.dennistschamler.survey.model.entity.survey;

import cz.dennistschamler.survey.model.dto.SurveyAnswerDto;
import cz.dennistschamler.survey.model.dto.SurveyDto;
import cz.dennistschamler.survey.model.entity.BaseService;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ISurveyService extends BaseService<Survey, Integer> {
    Survey findLatestSurvey();
    Survey getByIdAndSlug(Integer id, String slug);
    long countAllActive();
    List<Survey> findNewestSurveys();
    List<Survey> getPaginatedSurveys(Pageable pageable);
    List<Survey> getPaginatedActiveSurveys(Pageable pageable);
    List<Survey> getSoonToBeExpiredSurveys();
    void create(SurveyDto surveyDto);
    void edit(SurveyDto surveyDto);
    void submitSurvey(SurveyAnswerDto surveyAnswerCreate) throws SurveyAlreadySubmittedException;
}
