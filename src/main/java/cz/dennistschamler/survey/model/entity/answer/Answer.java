package cz.dennistschamler.survey.model.entity.answer;

import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.model.entity.user_anwser.UserAnswer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="answers")
public class Answer {

    @Id
    @GeneratedValue()
    private int id;

    @Column(nullable = false)
    private String label;

    @Column(nullable = false)
    private int sort;

    @ManyToOne
    private Question question;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "answer", fetch = FetchType.EAGER)
    private Set<UserAnswer> userAnswers = new HashSet<>();

    public Answer() {

    }

    public void addUserAnswer(UserAnswer userAnswer) {
        userAnswers.add(userAnswer);
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Set<UserAnswer> getUserAnswers() {
        return userAnswers;
    }

    public void setUserAnswers(Set<UserAnswer> userAnswers) {
        this.userAnswers = userAnswers;
    }

    public int getSort() {
        return sort;
    }


    public void setSort(int sort) {
        this.sort = sort;
    }
}
