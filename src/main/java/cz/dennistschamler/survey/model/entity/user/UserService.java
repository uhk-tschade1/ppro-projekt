package cz.dennistschamler.survey.model.entity.user;

import cz.dennistschamler.survey.model.dto.UserSignUpDto;
import cz.dennistschamler.survey.model.entity.role.Role;
import cz.dennistschamler.survey.model.entity.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    private UserRepository userRepository;

    private RoleService roleService;

    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User register(UserSignUpDto userSignUpDto)
            throws UserAlreadyExistException {
        if (emailExist(userSignUpDto.getEmail())) {
            throw new UserAlreadyExistException(
                    "There is an account with that email address: "
                            +  userSignUpDto.getEmail());
        }

        User user = new User();

        user.setFirstName(userSignUpDto.getFirstName());
        user.setLastName(userSignUpDto.getLastName());
        user.setPassword(passwordEncoder.encode(userSignUpDto.getPassword()));
        user.setEmail(userSignUpDto.getEmail());

        Role role = roleService.findByName("ROLE_MEMBER");
        user.setRole(role);

        save(user);

        return user;
    }

    @Override
    public User findLatestUser() {
        return userRepository.findFirstByOrderByCreatedAtDesc();
    }

    @Override
    public List<User> getPaginatedUsers(Pageable pageable) {
        return userRepository.findAll(pageable).getContent();
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void saveAll(Iterable<User> entities) {
        userRepository.saveAll(entities);
    }

    private boolean emailExist(String email) {
        return userRepository.findOneByEmail(email) != null;
    }

    @Override
    public User findOneByEmail(String email) {
        return userRepository.findOneByEmail(email);
    }

    @Override
    public User getAuthenticatedUser(Principal principal) {
        if(principal == null) {
            return null;
        }

        String currentPrincipalName = principal.getName();

        return this.userRepository.findOneByEmail(currentPrincipalName);
    }

    @Override
    public boolean alreadySubmittedSurvey(Integer surveyId, Integer userId) {
        return userRepository.alreadySubmittedSurvey(surveyId, userId);
    }

    @Override
    public boolean canDeleteUser(Integer userId) {
        User user = userRepository.getOne(userId);

        if(user == null) {
            return false;
        }

        if(user.getRole().getName().equals("ROLE_ADMIN")) {
           return false;
        }

        return user.getUserAnswers().isEmpty();
    }

    @Override
    public List<User> getUsersBySurvey(Integer surveyId) {
        return userRepository.findUsersBySurvey(surveyId);
    }

    @Override
    public Optional<User> findById(Integer userId) {
        return userRepository.findById(userId);
    }

    @Override
    public User getOne(Integer id) {
        return userRepository.getOne(id);
    }

    @Override
    public long countAll() {
        return userRepository.count();
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public List<User> findFirst10ByOrderByCreatedAtDesc() {
        return userRepository.findFirst10ByOrderByCreatedAtDesc();
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setPasswordEncoder(@Lazy PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
