package cz.dennistschamler.survey.model.entity.question;

public enum QuestionType {
    SINGLE_TEXT, MULTIPLE_CHOICE, SINGLE_CHOICE
}
