package cz.dennistschamler.survey.model.entity.user;

import cz.dennistschamler.survey.model.dto.UserSignUpDto;
import cz.dennistschamler.survey.model.entity.BaseService;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;

public interface IUserService extends BaseService<User, Integer> {
    User register(UserSignUpDto userSignUpDto) throws UserAlreadyExistException;
    User getAuthenticatedUser(Principal principal);
    User findLatestUser();
    List<User> getUsersBySurvey(Integer surveyId);
    List<User> findFirst10ByOrderByCreatedAtDesc();
    User findOneByEmail(String email);
    List<User> getPaginatedUsers(Pageable pageable);
    boolean canDeleteUser(Integer userId);
    boolean alreadySubmittedSurvey(Integer surveyId, Integer userId);
}
