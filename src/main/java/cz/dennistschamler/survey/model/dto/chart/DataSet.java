package cz.dennistschamler.survey.model.dto.chart;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class DataSet {
    private List<Number> data = new ArrayList<>();
    private List<String> backgroundColor = new ArrayList<>();
    private List<String> label = new ArrayList<>();

    public void addDataPoint(Number n, String l) {
        data.add(n);
        label.add(l);

        Color color = ChartColor.random(backgroundColor.size());
        backgroundColor.add(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
    }

    public List<Number> getData() {
        return data;
    }

    public void setData(List<Number> data) {
        this.data = data;
    }

    public List<String> getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(List<String> backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public List<String> getLabel() {
        return label;
    }

    public void setLabel(List<String> label) {
        this.label = label;
    }
}
