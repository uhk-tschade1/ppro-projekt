package cz.dennistschamler.survey.model.dto.chart;

public class PieChart extends Chart {
    private PieChartData data;

    public PieChart() {
        super(ChartType.PIE);
    }

    public PieChartData getData() {
        return data;
    }

    public void setData(PieChartData data) {
        this.data = data;
    }
}
