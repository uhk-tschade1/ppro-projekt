package cz.dennistschamler.survey.model.dto;

import javax.validation.constraints.NotBlank;

public class UserSignInDto {

    @NotBlank
    private String email;

    @NotBlank
    private String password;

    @NotBlank
    private boolean rememberMe;

    public UserSignInDto() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    @Override
    public String toString() {
        return "SignInUser{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
