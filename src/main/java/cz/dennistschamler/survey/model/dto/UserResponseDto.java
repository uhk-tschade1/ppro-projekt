package cz.dennistschamler.survey.model.dto;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class UserResponseDto {

    @NotEmpty
    private List<Integer> answers = new ArrayList<>();

    @NotEmpty(message = "Response has to be filled")
    private List<String> responses = new ArrayList<>();

    public List<String> getResponses() {
        return responses;
    }

    public void setResponses(List<String> responses) {
        this.responses = responses;
    }

    public List<Integer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Integer> answers) {
        this.answers = answers;
    }
}
