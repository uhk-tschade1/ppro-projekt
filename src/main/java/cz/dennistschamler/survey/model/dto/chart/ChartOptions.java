package cz.dennistschamler.survey.model.dto.chart;

public class ChartOptions {
    private Boolean maintainAspectRatio = false;

    public Boolean getMaintainAspectRatio() {
        return maintainAspectRatio;
    }

    public void setMaintainAspectRatio(Boolean maintainAspectRatio) {
        this.maintainAspectRatio = maintainAspectRatio;
    }
}
