package cz.dennistschamler.survey.model.dto;

import cz.dennistschamler.survey.model.validation.PasswordMatches;

import javax.validation.constraints.NotBlank;

@PasswordMatches
public class UserSignUpDto {
    @NotBlank(message="First name has to be filled")
    private String firstName;

    @NotBlank(message="Last name has to be filled")
    private String lastName;

    @NotBlank(message="Password has to be filled")
    private String password;

    @NotBlank(message="Matching password has to be filled")
    private String matchingPassword;

    @NotBlank(message="E-mail has to be filled")
    private String email;

    public UserSignUpDto() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "SignUpUser{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", matchingPassword='" + matchingPassword + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
