package cz.dennistschamler.survey.model.dto;

import cz.dennistschamler.survey.model.validation.FutureDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SurveyDto {
    private Integer id;

    @NotBlank(message = "Name has to be filled")
    private String name;

    @NotBlank(message = "Description has to be filled")
    private String description;

    @NotNull(message = "End date needs to be specified")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @FutureDate(message = "End date needs to be set in the future")
    private Date endsAt;

    @Valid
    @NotEmpty(message = "Add at least one question")
    private List<QuestionDto> questions = new ArrayList<>();

    public SurveyDto() {

    }

    public void addQuestion(QuestionDto questionDto) {
        questions.add(questionDto);
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<QuestionDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDto> questions) {
        this.questions = questions;
    }

    public Date getEndsAt() {
        return endsAt;
    }

    public void setEndsAt(Date endsAt) {
        this.endsAt = endsAt;
    }
}
