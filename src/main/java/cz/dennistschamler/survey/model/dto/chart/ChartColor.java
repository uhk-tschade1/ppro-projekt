package cz.dennistschamler.survey.model.dto.chart;

import cz.dennistschamler.survey.utils.HslColor;

import java.awt.*;

public class ChartColor {
    public static Color random(int number) {
        float hue = number * 137.508f;
        return HslColor.toRGB(hue, 50, 50);
    }
}
