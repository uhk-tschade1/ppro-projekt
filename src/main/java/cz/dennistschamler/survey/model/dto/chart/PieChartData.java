package cz.dennistschamler.survey.model.dto.chart;

import java.util.ArrayList;
import java.util.List;

public class PieChartData {
    private List<DataSet> datasets = new ArrayList<>();
    private List<String> labels = new ArrayList<>();

    public void addDataSet(DataSet dataSet) {
        datasets.add(dataSet);
        labels = dataSet.getLabel();
    }

    public List<DataSet> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<DataSet> datasets) {
        this.datasets = datasets;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }
}
