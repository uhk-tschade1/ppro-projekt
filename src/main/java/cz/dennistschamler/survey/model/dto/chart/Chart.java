package cz.dennistschamler.survey.model.dto.chart;

public class Chart {
    protected ChartType type;
    protected ChartOptions options = new ChartOptions();

    public Chart(ChartType type) {
        this.type = type;
    }

    public ChartType getType() {
        return type;
    }

    public void setType(ChartType type) {
        this.type = type;
    }

    public ChartOptions getOptions() {
        return options;
    }

    public void setOptions(ChartOptions options) {
        this.options = options;
    }
}
