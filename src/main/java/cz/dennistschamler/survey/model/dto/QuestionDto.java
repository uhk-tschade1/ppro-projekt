package cz.dennistschamler.survey.model.dto;

import cz.dennistschamler.survey.model.entity.question.QuestionType;
import cz.dennistschamler.survey.model.validation.QuestionSingleTextOneAnswer;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@QuestionSingleTextOneAnswer
public class QuestionDto {

    private Integer id;

    @NotBlank(message="Label has to be filled")
    private String label;

    @Enumerated(EnumType.STRING)
    private QuestionType type;

    @Valid
    @NotEmpty(message = "Question needs to have at least one answer")
    private List<AnswerDto> answers = new ArrayList<>();

    @NotNull
    private int sort;

    public QuestionDto() {

    }

    public void addAnswer(AnswerDto answerDto) {
        this.answers.add(answerDto);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public List<AnswerDto> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDto> answers) {
        this.answers = answers;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
