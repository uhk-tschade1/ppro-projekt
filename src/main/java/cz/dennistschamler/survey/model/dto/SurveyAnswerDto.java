package cz.dennistschamler.survey.model.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class SurveyAnswerDto {

    @Valid
    @NotEmpty
    private List<UserResponseDto> userResponses = new ArrayList<>();

    @NotNull
    private int userId;

    @NotNull
    private int surveyId;

    public SurveyAnswerDto() {

    }

    public void addUserResponse(UserResponseDto userResponse) {
        userResponses.add(userResponse);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setsurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public List<UserResponseDto> getUserResponses() {
        return userResponses;
    }

    public void setUserResponses(List<UserResponseDto> userResponses) {
        this.userResponses = userResponses;
    }
}
