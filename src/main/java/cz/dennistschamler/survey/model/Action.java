package cz.dennistschamler.survey.model;

public class Action {

    private String label;
    private String icon;
    private String link;
    private String color;

    public Action(String label, String icon, String link) {
        this(label, icon, link, "secondary");
    }

    public Action(String label, String icon, String link, String color) {
        this.label = label;
        this.icon = icon;
        this.link = link;
        this.color = color;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getColor() {
        return color;
    }
}
