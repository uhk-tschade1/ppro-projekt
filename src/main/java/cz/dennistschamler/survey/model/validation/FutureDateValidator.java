package cz.dennistschamler.survey.model.validation;

import cz.dennistschamler.survey.utils.DateUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

public class FutureDateValidator
        implements ConstraintValidator<FutureDate, Date> {

    private static final Date CURRENT_DATE = DateUtils.getCurrentDateWithoutTime();

    @Override
    public void initialize(FutureDate constraintAnnotation) {
    }
    @Override
    public boolean isValid(Date futureDate, ConstraintValidatorContext context){
        if(futureDate == null) {
            return false;
        }

        return CURRENT_DATE.before(futureDate) || CURRENT_DATE.compareTo(futureDate) == 0;
    }

}