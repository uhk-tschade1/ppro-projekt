package cz.dennistschamler.survey.model.validation;

import cz.dennistschamler.survey.model.dto.UserResponseDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Map;

public class MapResponseValidator implements
        ConstraintValidator<MapResponse, Map<String, List<UserResponseDto>>> {

    @Override
    public void initialize(MapResponse mapResponse) {
    }

    @Override
    public boolean isValid(Map<String, List<UserResponseDto>> mapResponse,
                           ConstraintValidatorContext cxt) {

        boolean isValid = true;


        for (Map.Entry<String, List<UserResponseDto>> e : mapResponse.entrySet()) {
            List<UserResponseDto> responses = e.getValue();

            if (responses == null || responses.size() == 0) {
                isValid = false;
                break;
            }
        }

        return isValid;
    }

}
