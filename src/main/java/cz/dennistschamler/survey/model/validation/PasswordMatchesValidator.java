package cz.dennistschamler.survey.model.validation;

import cz.dennistschamler.survey.model.dto.UserSignUpDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        UserSignUpDto user = (UserSignUpDto) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}
