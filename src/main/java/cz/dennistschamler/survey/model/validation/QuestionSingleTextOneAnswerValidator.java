package cz.dennistschamler.survey.model.validation;

import cz.dennistschamler.survey.model.dto.QuestionDto;
import cz.dennistschamler.survey.model.entity.question.QuestionType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuestionSingleTextOneAnswerValidator
        implements ConstraintValidator<QuestionSingleTextOneAnswer, Object> {

    @Override
    public void initialize(QuestionSingleTextOneAnswer constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        QuestionDto questionDto = (QuestionDto) obj;

        if(questionDto.getType().equals(QuestionType.SINGLE_TEXT)) {
            return questionDto.getAnswers().size() == 1;
        }

        return true;
    }
}
