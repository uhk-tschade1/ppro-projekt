package cz.dennistschamler.survey.model;

public class Breadcrumb {
    private String label;
    private String link;

    public Breadcrumb(String label, String link) {
        this.label = label;
        this.link = link;
    }

    public Breadcrumb(String label) {
        this(label, "#");
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
