package cz.dennistschamler.survey.model;


public class FlashMessage {
    private String type;
    private String message;

    public static String KEY = "message";

    public FlashMessage(String message, String type) {
        this.message = message;
        this.type = type;
    }

    public FlashMessage(String message) {
        this( message, "info");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
