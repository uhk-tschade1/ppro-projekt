package cz.dennistschamler.survey.model.xlsx;

import cz.dennistschamler.survey.model.entity.answer.Answer;
import cz.dennistschamler.survey.model.entity.question.Question;
import cz.dennistschamler.survey.model.entity.question.QuestionType;
import cz.dennistschamler.survey.model.entity.survey.Survey;
import cz.dennistschamler.survey.model.entity.user_anwser.UserAnswer;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Map;

public class SurveyXlsxView extends AbstractXlsxView {

    private XSSFCellStyle  surveyHeaderCellStyle, questionHeaderCellStyle, answerHeaderCellStyle;

    private final int SURVEY_ROW = 0;

    private final int QUESTION_ROW = 3;

    private void initStyles(Workbook workbook,Sheet sheet) {
        surveyHeaderCellStyle = createCellStyle(workbook, sheet, "E74C3C");
        questionHeaderCellStyle = createCellStyle(workbook, sheet, "0496ff");
        answerHeaderCellStyle = createCellStyle(workbook, sheet, "E9C46A");
    }

    private XSSFCellStyle createCellStyle(Workbook workbook,Sheet sheet, String backgroundColor) {
        XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();

        cellStyle.cloneStyleFrom(sheet.getColumnStyle(0));
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        Font font = workbook.createFont();
        font.setBold(true);
        cellStyle.setFont(font);

        try {
            byte[] rgb = Hex.decodeHex(backgroundColor);

            cellStyle.setFillForegroundColor(new XSSFColor(rgb, null));
        } catch (DecoderException ex) {
            cellStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        }

        return cellStyle;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        Sheet sheet = workbook.createSheet("Survey responses");

        Survey survey = (Survey) map.get("survey");

        initStyles(workbook, sheet);

        generateSurveyRowHeader(sheet);
        generateSurveyRow(sheet, survey);

        Collection<Question> questions = (Collection<Question>) survey.getQuestions();

        int row = 0;
        for (Question q : questions) {
            generateQuestionHeaderRow(sheet, row);
            generateQuestionRow(sheet, q, row);

            if(q.getType() == QuestionType.SINGLE_TEXT) {
                row+=3 + q.getAnswers().iterator().next().getUserAnswers().size() + 2;
            } else {
                row+=3 + q.getAnswers().size() + 2;
            }
        }

        for(int i = 0; i < 4; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private void generateSurveyRowHeader(Sheet sheet) {
        Row row = sheet.createRow(SURVEY_ROW);

        Cell surveyId = row.createCell(0);
        surveyId.setCellValue("Survey ID");
        surveyId.setCellStyle(surveyHeaderCellStyle);

        Cell surveyName = row.createCell(1);
        surveyName.setCellValue("Survey Name");
        surveyName.setCellStyle(surveyHeaderCellStyle);

        Cell surveyDescription = row.createCell(2);
        surveyDescription.setCellValue("Survey Description");
        surveyDescription.setCellStyle(surveyHeaderCellStyle);
    }

    private void generateSurveyRow(Sheet sheet, Survey survey) {
        Row row = sheet.createRow(SURVEY_ROW+1);

        Cell surveyId = row.createCell(0);
        surveyId.setCellValue(survey.getId());

        Cell surveyName = row.createCell(1);
        surveyName.setCellValue(survey.getName());

        Cell surveyDescription = row.createCell(2);
        surveyDescription.setCellValue(survey.getDescription());
    }

    private void generateQuestionHeaderRow(Sheet sheet, int r) {
        Row row = sheet.createRow(QUESTION_ROW+r);

        Cell questionId = row.createCell(0);
        questionId.setCellValue("Question ID");
        questionId.setCellStyle(questionHeaderCellStyle);

        Cell questionLabel = row.createCell(1);
        questionLabel.setCellValue("Question label");
        questionLabel.setCellStyle(questionHeaderCellStyle);

        Cell questionType = row.createCell(2);
        questionType.setCellValue("Question Type");
        questionType.setCellStyle(questionHeaderCellStyle);

        Cell questionNumOfAnswers = row.createCell(3);
        questionNumOfAnswers.setCellValue("Question number of answers");
        questionNumOfAnswers.setCellStyle(questionHeaderCellStyle);
    }

    private void generateQuestionRow(Sheet sheet, Question question, int r) {
        Row row = sheet.createRow(QUESTION_ROW + r + 1);

        Cell questionId = row.createCell(0);
        questionId.setCellValue(question.getId());

        Cell questionLabel = row.createCell(1);
        questionLabel.setCellValue(question.getLabel());

        Cell questionType = row.createCell(2);
        questionType.setCellValue(question.getType().toString());

        Cell questionNumOfAnswers = row.createCell(3);
        questionNumOfAnswers.setCellValue(question.getAnswers().size());

        int i = 2;

        if(question.getType() == QuestionType.SINGLE_TEXT) {
            Answer a = question.getAnswers().iterator().next();
            generateUserAnswerHeaderRow(sheet, QUESTION_ROW + r + 1 + i);
            for (UserAnswer userAnswer : a.getUserAnswers()) {
                generateUserAnswerRow(sheet, userAnswer, QUESTION_ROW + r + 1 + i + 1);
                i++;
            }
        } else {
            generateAnswerHeaderRow(sheet, QUESTION_ROW + r + 1 + i);
            for (Answer a : question.getAnswers()) {
                generateAnswerRow(sheet, a, QUESTION_ROW + r + 1 + i + 1);
                i++;
            }
        }
    }

    private void generateAnswerHeaderRow(Sheet sheet, int r) {
        Row row = sheet.createRow(r);

        Cell answerId = row.createCell(0);
        answerId.setCellValue("Answer ID");
        answerId.setCellStyle(answerHeaderCellStyle);

        Cell answerLabel = row.createCell(1);
        answerLabel.setCellValue("Answer label");
        answerLabel.setCellStyle(answerHeaderCellStyle);

        Cell answerNumOfResponses = row.createCell(2);
        answerNumOfResponses.setCellValue("Answer number of responses");
        answerNumOfResponses.setCellStyle(answerHeaderCellStyle);
    }

    private void generateAnswerRow(Sheet sheet, Answer answer, int r) {
        Row row = sheet.createRow(r);

        Cell answerId = row.createCell(0);
        answerId.setCellValue(answer.getId());

        Cell answerLabel = row.createCell(1);
        answerLabel.setCellValue(answer.getLabel());

        Cell answerUserAnswersCount = row.createCell(2);
        answerUserAnswersCount.setCellValue(answer.getUserAnswers().size());
    }

    private void generateUserAnswerHeaderRow(Sheet sheet, int r) {
        Row row = sheet.createRow(r);

        Cell answerId = row.createCell(0);
        answerId.setCellValue("User Answer ID");
        answerId.setCellStyle(answerHeaderCellStyle);

        Cell answerLabel = row.createCell(1);
        answerLabel.setCellValue("User Answer response");
        answerLabel.setCellStyle(answerHeaderCellStyle);
    }

    private void generateUserAnswerRow(Sheet sheet, UserAnswer userAnswer, int r) {
        Row row = sheet.createRow(r);

        Cell userAnswerId = row.createCell(0);
        userAnswerId.setCellValue(userAnswer.getId());

        Cell userAnswerResponse = row.createCell(1);
        userAnswerResponse.setCellValue(userAnswer.getResponse());
    }

    /*private Cell generateCell(Row row, int index, CellStyle cellStyle) {
        Cell surveyId = surveyRow.createCell(0);
        surveyId.setCellValue(survey.getId());
        surveyId.setCellStyle(surveyCellStyle);
    }*/
}
