## Localhost

### Instalace závislosti

 Pomocí npm
- `npm install`
- `npm run scss`

Pomocí mvn
- `mvn com.github.eirslett:frontend-maven-plugin:1.11.0:npm`

### Spustit na localhostu
- `mvn spring-boot:run -Dspring-boot.run.profiles=local` - předpokládá existenci databáze **survey** na localhost:3306


## Docker

- `docker-compose up`
- Aplikace je dostupná na http://localhost:8081
- PHPMyAdmin - http://localhost:8080 (root:root)

## Výchozí živatelé

| E-mail | Password | Role | Povolené akce |
| ------ | ------ | ------ | ------- |
| admin@admin.cz | admin | ADMIN | Vše |
| member@member.cz | member | MEMBER | Nemá přístup do administrace, může vyplňovat dotazníky |
